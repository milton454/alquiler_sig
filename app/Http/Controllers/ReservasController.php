<?php

namespace App\Http\Controllers;

use App\reservas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ReservasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['reservas']=Reservas::paginate(5);

        return view('reservas.index',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reservas.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[
            'nombre'=> 'required|string|max:100',
            'apellido'=> 'required|string|max:100',
            'direcion'=> 'required|string|max:100',
            'edad'=> 'required|integer',
            'dias'=> 'required|integer',
            'fecha'=> 'required|date',
            'tipopago'=> 'required|string|max:100',
            'mauto'=> 'required|string|max:100'
        ];
        $Mensaje=["required"=>'El:attribute es requerido'];
        $this->validate($request,$campos,$Mensaje);

        //$datosReserva=request()->all();
        $datosReserva=request()->except('_token');
        Reservas::insert($datosReserva);

        //return response()->json($datosReserva);
        return redirect('reservas')->with('Mensaje','Se agrego nueva reserva con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\reservas  $reservas
     * @return \Illuminate\Http\Response
     */
    public function show(reservas $reservas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\reservas  $reservas
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $reserva=Reservas::findOrFail($id);
        return view('reservas.edit',compact('reserva'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\reservas  $reservas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $campos=[
            'nombre'=> 'required|string|max:100',
            'apellido'=> 'required|string|max:100',
            'direcion'=> 'required|string|max:100',
            'edad'=> 'required|integer',
            'dias'=> 'required|integer',
            'fecha'=> 'required|date',
            'tipopago'=> 'required|string|max:100',
            'mauto'=> 'required|string|max:100'
        ];
        $Mensaje=["required"=>'El:attribute es requerido'];
        $this->validate($request,$campos,$Mensaje);

        $datosReserva=request()->except(['_token','_method']);

        Reservas::where('id','=',$id)->update($datosReserva);

        //$reserva=Reservas::findOrFail($id);
        //return view('reservas.edit',compact('reserva'));
        return redirect('reservas')->with('Mensaje','Se modifico exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\reservas  $reservas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Reservas::destroy($id);
        return redirect('reservas')->with('Mensaje','Se elimino exitosamente');
    }
}
