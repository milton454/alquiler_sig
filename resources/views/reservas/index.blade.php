@extends('layouts.app')

@section('content')

<div class="container">

@if(Session::has('Mensaje')){{
    Session::get('Mensaje')
}}
@endif
<h1>TABLA DE REGISTRO DE RESERVAS</h1>
<a href="{{url('reservas/create')}}" class="btn btn-primary">Agregar nueva reserva</a>
<br>
<br>
<table class="table table-light table-hover">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Direccion</th>
            <th>Edad</th>
            <th>Dias</th>
            <th>Fecha</th>
            <th>Tipo de Pago</th>
            <th>Marca de Auto</th>
            <th>Acciones</th>
        </tr>
    </thead>

    <tbody>
    @foreach($reservas as $reserva)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$reserva-> nombre}}</td>
            <td>{{$reserva-> apellido}}</td>
            <td>{{$reserva-> direcion}}</td>
            <td>{{$reserva-> edad}}</td>
            <td>{{$reserva-> dias}}</td>
            <td>{{$reserva-> fecha}}</td>
            <td>{{$reserva-> tipopago}}</td>
            <td>{{$reserva-> mauto}}</td>
            <td>
            <a class="btn btn-success" href="{{ url('/reservas/'.$reserva->id.'/edit')}}">
            Modificar
            </a> 
            <form method="post" action="{{url('/reservas/'.$reserva->id)}}"style="display:inline">
            {{csrf_field()}}
            {{method_field('DELETE')  }}
            <button class="btn btn-danger" type="submit" onclick="return confirm('confirmar');">Eliminar</button>
            </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

</div>

@endsection