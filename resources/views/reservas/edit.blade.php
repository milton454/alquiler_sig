@extends('layouts.app')

@section('content')

<div class="container">

<!doctype html>
<html lang="en">
  <head>
    <!--  meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Use border utilities to quickly style the border and border-radius of an element. Great for images, buttons, or any other element.">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<div class="card border-primary mb-3">
    <div class="row no-gutters">
        <div class="col-md-4">
            <img src="https://goo.gl/maps/qb2WGYqBNkDz1mzV8" class="card-img" alt="..." style="max-height: 100%">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15230.206178007169!2d-66.2010302!3d-17.3852993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2sbo!4v1563157761691!5m2!1ses!2sbo" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <div class=" card text-black  col-md-8">
            <div class="card-body py-5">
                <form action="{{url('/reservas/'.$reserva->id)}}" method="post" enctype="multipart/form-data" >
                {{csrf_field()}}
                {{method_field('PATCH')}}
                <h1><b>REGISTRO DE RESERVAS</b></h1>
                <div class="form-row">
                    <div class="col-md-5 mb-2">
                        <label for="nombre">{{'Nombre'}}</label>
                        <input type="text" class="form-control" placeholder="ingrese su nombre" name="nombre" value="{{$reserva->nombre}}">
                    </div>
                    <div class="col-md-5 mb-2">
                        <label for="apellido">{{'Apellido'}}</label>
                        <input type="text" class="form-control" placeholder="ingrese su apelldio" name="apellido" value="{{$reserva->apellido}}" >
                    </div>
                    <div class="col-md-5 mb-2">
                        <label for="direcion">{{'Direcion'}}</label>
                        <input type="text" class="form-control" placeholder="ingrese su direccion" name="direcion" value="{{$reserva->direcion}}">
                    </div>
            </div>
                <div class="form-row">
                    <div class="col-md-5 mb-2">
                        <label for="edad">{{'Edad'}}</label>
                        <input type="text" class="form-control" placeholder="ingrese su edad" name="edad" value="{{$reserva->edad}}">                      
                    </div>
                    <div class="col-md-5 mb-2">
                        <label for="dias" placeholder="maximo de 10 dias">{{'Dias'}}</label>
                        <select class="form-control" name="dias" value="{{$reserva->dias}}">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                        </select>
                    </div>
                    <div class="col-md-5 mb-2">
                        <label for="fecha">{{'Fecha'}}</label>
                        <input type="date" class="form-control" name="fecha" value="{{$reserva->fecha}}">
                    </div>
                </div>
                    <div class="form-row">
                        <div class="col-md-5 mb-2">
                            <label for="tipopago">{{'Tipo Pago'}}</label>
                            <select class="form-control" name="tipopago" value="{{$reserva->tipopago}}">
                                <option>TARJETA</option>
                                <option>EFECTIVO</option>
                                <option>CHEQUE</option>
                            </select>                      
                        </div>
                        <div class="col-md-5 mb-2">
                            <label for="mauto">{{'Marca Auto'}}</label>
                            <select class="form-control" name="mauto" value="{{$reserva->mauto}}">
                                <option>BMW</option>
                                <option>TOYOTA</option>
                                <option>COROLLA</option>
                                <option>FORD</option>
                            </select>
                        </div>
                    </div>       
                    <button class="btn btn-primary" type="submit" value="Editar">MODIFICAR</button>
                    <a class="btn btn-primary" href="{{url('reservas')}}">ATRAS</a>
                </form>
            </div>        
        </div>
    </div>
</div> 

</div>

@endsection